<div class="bd-example" style="width:100%; height: 280px; " >
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" style="height:100%; height: 280px">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <?php
              $cont = 1;    
                foreach ($produtosGaleria as $d) {
              
                    
            ?>
            <div class="carousel-item <?= $cont == 1 ? "active": "" ?>">
                <img src="<?= HOST . '/img/' . $d['img'] ?>" width="100%" height= "290px" class="d-block w-100" alt="<?= $d['nm'] ?>">
                <div class="carousel-caption d-none d-md-block  ">
                    <h5><?= $d['nm'] ?></h5>
                    <p><?= substr($d['descricao'], 0, 100) . " ..." ?></p>
                </div>
            </div>
            <?php
            $cont = 0;
                }
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>


<!-- Cards -->
<br>

<center>Vitrine</center>
<div class="card-deck">
    <?php
    $cont = 0;
    foreach ($produtosHome as $d) {
        ?>

        <div class="card">
            <img class="card-img-top" src="<?= HOST . '/img/' . $d['img'] ?>" height="50%" style="min-height: 250px;" alt="Imagem de capa do card">
            <div class="card-body">
                <h5 class="card-title"><?= $d['nm'] ?></h5>
                <p class="card-text"><?= substr($d['descricao'], 0, 200) . " ..." ?></p>
                <p class="  "><small class="text-dark" style="font-size: 30px;">R$ <?= $d['vr'] ?> </small> <a href="<?= HOST.'/produtos/comprar/'.str_replace(" ", "-", $d['nm'])."-".$d['id'];  ?>"><small class="  btn btn-success" style="font-size: 20px;">COMPRAR </small> </a></p>
            </div>
        </div>
        <?php
        $cont++;
        if ($cont == 3) {
            echo '<div><br>';

            echo '<div class="card-deck">';
            $cont = 0;
        }
    }
    ?>
</div>

<br>
<center>Kits</center>
<div class="card-deck">
    <?php
    $cont = 0;
    foreach ($KitsHome as $d) {
        ?>

        <div class="card">
            <img class="card-img-top" src="<?= HOST . '/img/' . $d['img'] ?>" height="50%" style="min-height: 250px;" alt="Imagem de capa do card">
            <div class="card-body">
                <h5 class="card-title"><?= $d['nm'] ?></h5>
                <p class="card-text"><?= substr($d['descricao'], 0, 150) . " ..." ?></p>
                <p class="  "><small class="text-dark" style="font-size: 30px;">R$ <?= $d['vr'] ?> </small> <a href="<?= HOST.'/produtos/comprar/'.str_replace(" ", "-", $d['nm'])."-".$d['id'];  ?>"><small class="  btn btn-success" style="font-size: 20px;">COMPRAR </small> </a></p>
            </div>
        </div>
        <?php
        $cont++;
        if ($cont == 3) {
            echo '<div>';
            echo '<div class="card-deck">';
            $cont = 0;
        }
    }
    ?>
</div>
</div>