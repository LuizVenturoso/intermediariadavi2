
<div class="card-deck">
    <?php
    $cont = 0;
    foreach ($produtosComprar as $d) {
        ?>

        <div class="card">
            <img class="card-img-top" src="<?= HOST . '/img/' . $d['img'] ?>" height="50%" style="min-height: 250px;" alt="Imagem de capa do card">
            <div class="card-body">
                <h5 class="card-title"><?= $d['nm'] ?></h5>
                <p class="card-text"><?= substr($d['descricao'], 0, 200) . " ..." ?></p>
                <p class="  "><small class="text-dark" style="font-size: 30px;">R$ <?= $d['vr'] ?> </small> <a href="<?= HOST.'/produtos/finalizar/'.str_replace(" ", "-", $d['nm'])."-".$d['id'];  ?>"><small class="  btn btn-success" style="font-size: 20px;">COMPRAR </small> </a></p>
            </div>
        </div>
        <?php
        $cont++;
        if ($cont == 3) {
            echo '<div><br>';

            echo '<div class="card-deck">';
            $cont = 0;
        }
    }
    ?>
</div>

